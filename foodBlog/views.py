# Create your views here.
import uuid

from django.http import HttpResponse
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from foodBlog.models import User, Blog
from foodBlog.serilizer import User_Serializer, Blog_Serializer
from foodBlog.utils import requestContains


@api_view(['POST', 'GET'])
def serverHealth(request):
    if request.method == 'POST':
        return Response("Server ok Post Request", status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
    else:
        return HttpResponse("Server ok GET Request")


def isUserExist(email):
    return User.objects.filter(email=email).exists()


# Credential
@api_view(['GET'])
def loginRender(request):
    return render(request, "login.html")


@api_view(['GET'])
def registerRender(request):
    return render(request, "register.html")


@api_view(['POST'])
def login(request):
    if requestContains(request.data, ['email', 'password']):
        if User.objects.filter(email=request.data['email'], password=request.data['password']).exists():
            request.session['sid'] = request.data['email']
            return redirect("/home")
        else:
            return render(request, "login.html", context={"message": "Invalid Credentials"})
    else:
        return render(request, "login.html", context={"message": "Invalid Input"})


@api_view(['POST'])
def register(request):
    if requestContains(request.data, ['name', 'email', 'password']):
        if not isUserExist(request.data['email']):
            data = {
                "id": uuid.uuid4().hex,
                "name": request.data['name'],
                "email": request.data['email'],
                "password": request.data['password']
            }
            callback = User_Serializer(data=data, many=False)
            if callback.is_valid():
                callback.save()
                request.session['sid'] = request.data['email']
                return redirect("/home")
            else:
                return render(request, "register.html", context={"message": "Error while Creating User"})
        else:
            return render(request, "register.html", context={"message": "User already Exist"})
    else:
        return render(request, "register.html", context={"message": "Invalid Inputs"})


# Blog
def allBlog(request):
    if request.session.get('sid', False):
        return render(request, "blog.html", context={"user": request.session.get('sid'), "blogs": getAllBlog()})
    else:
        return redirect("/login")


def getAllBlog():
    return Blog.objects.all()


@api_view(['POST'])
def createBlog(request):
    if request.session.get('sid', False):

        data = {
            "id": uuid.uuid4().hex,
            "author": request.session.get('sid'),
            "title": request.data['title'],
            "description": request.data['description']
        }

        callback = Blog_Serializer(data=data, many=False)

        if callback.is_valid():
            callback.save()
            return render(request, "blog.html", context={"user": request.session.get('sid'), "message": "Blog Created",
                                                         "blogs": getAllBlog()})
        else:
            return render(request, "blog.html",
                          context={"user": request.session.get('sid'), "message": "Error Creating Blog",
                                   "blogs": getAllBlog()})
    else:
        return HttpResponse('Not Logged IN')


@api_view(['POST'])
def updateBlog(request):
    if request.session.get('sid', False) and requestContains(request.data, ['id', 'title', 'description']):

        obj_instance = Blog.objects.get(id=request.data['id'])
        obj_instance.title = request.data['title']
        obj_instance.description = request.data['description']

        obj_instance.save(update_fields=['title', 'description'])

        return render(request, "blog.html",
                      context={"user": request.session.get('sid'), "message": "Blog Updated Successfully",
                               "blogs": getAllBlog()})
    else:
        return HttpResponse('Not Logged IN')


@api_view(['POST'])
def deleteBlog(request):
    if request.session.get('sid', False) and requestContains(request.data, ['id']):

        Blog.objects.filter(id=request.data['id'], author=request.session.get('sid')).delete()

        return render(request, "blog.html",
                      context={"user": request.session.get('sid'), "message": "Blog Deleted Successfully",
                               "blogs": getAllBlog()})
    else:
        return HttpResponse('Not Logged IN')


def logout(request):
    del request.session['sid']
    return redirect("/login")
