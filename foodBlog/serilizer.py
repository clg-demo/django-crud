from rest_framework import serializers

from foodBlog.models import User, Blog


class User_Serializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class Blog_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = '__all__'
