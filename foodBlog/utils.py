def requestContains(request, need):
    return all(elem in request for elem in need)
