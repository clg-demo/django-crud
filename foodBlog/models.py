from django.db import models


# Create your models here.
class User(models.Model):
    id = models.CharField(primary_key=True, max_length=40)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)

    def __str__(self):
        return str(self.id) + "  " + str(self.name)


class Blog(models.Model):
    id = models.CharField(primary_key=True, max_length=40)
    author = models.CharField(max_length=50)
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)
