from django.urls import path

from foodBlog import views

urlpatterns = [
    # path("", views.serverHealth, name="Server Health"),

    path("", views.loginRender, name="login"),
    path("login", views.loginRender, name="login"),
    path("m-login", views.login, name="login"),

    path("register", views.registerRender, name="register"),
    path("m-register", views.register, name="register"),

    path("home", views.allBlog, name="home"),

    path("create-blog", views.createBlog, name="Create New Blog"),

    path("update-blog", views.updateBlog, name="Update Blog"),
    path("delete-blog", views.deleteBlog, name="Delete Blog"),
    path("logout", views.logout, name="Logout")
]
